import { countBy, isFinite, sumBy } from "lodash/fp";

import { isNotNil, prisma, StoreData } from "~/src/utils";

export const getBusinessModels = async () => {
  return await prisma.businessModel.findMany();
};

export const getAllStores = async (): Promise<StoreData[]> => {
  const stores = await prisma.store.findMany();
  const storeData: StoreData[] = [];

  for (let i = 0; i < stores.length; i++) {
    const storeId = stores[i].id;

    const services = await prisma.serviceProduct.findMany({
      where: {
        storeId,
      },
    });

    const products = await prisma.product.findMany({
      where: {
        storeId,
      },
    });

    const ratings = await prisma.rating.findMany({
      where: { target: storeId },
    });

    const totalRating =
      countBy(({ rating }) => isNotNil(rating), ratings).true ?? 0;
    const averageRating = sumBy("rating", ratings) / totalRating;

    storeData.push({
      ...stores[i],
      averageRating: isFinite(averageRating) ? averageRating : 0,
      totalService: services.length,
      totalProduct: products.length,
    });
  }

  return storeData;
};
