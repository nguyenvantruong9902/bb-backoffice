import { useEffect, useState } from "react";
import { ColumnsType } from "antd/lib/table";
import { SerializeFrom } from "@remix-run/node";
import { useNavigate } from "@remix-run/react";
import { BusinessModel } from "@prisma/client";
import { filter } from "lodash/fp";

import { Layout } from "~/src/components/organisms/Layout";
import {
  Text,
  Table,
  Avatar,
  CheckableTag,
  Switch,
  Input,
  Dropdown,
  DeleteIcon,
  EditIcon,
  SearchIcon,
  CalendarIcon,
  StoreIcon,
  PlusIcon,
} from "~/src/components/molecules";
import { RoutePath, StoreData, toNonAccentVietnamese } from "~/src/utils";
import { theme } from "~/src/common/config";

import {
  StyledWrapAvatar,
  StyledWrapName,
  StyledWrapBusinessTags,
  StyledWrapFilters,
  StyledWrapDropdownFilters,
  StyledWrapAction,
  StyledAddButton,
} from "./Stores.styles";

interface StoreProps {
  stores: SerializeFrom<StoreData[]>;
  businessModels: SerializeFrom<BusinessModel[]>;
}

const columns: ColumnsType<StoreData> = [
  {
    key: "name",
    dataIndex: "name",
    render: (value, record) => (
      <StyledWrapName>
        <StyledWrapAvatar>
          <Avatar src={record.avatar} size={42} shape="circle" />
        </StyledWrapAvatar>

        <div>
          <Text>{value}</Text>
          <Text variant="tiny">{record.address}</Text>
        </div>
      </StyledWrapName>
    ),
  },
  {
    key: "averageRating",
    dataIndex: "averageRating",
    render: (value) => (
      <div>
        <Text>{value} ⭐</Text>
        <Text variant="tiny">Nail Salon</Text>
      </div>
    ),
  },
  {
    key: "phone",
    dataIndex: "phone",
    render: (value) => (
      <div>
        <Text>{value}</Text>
        <Text variant="tiny">Anh Dũng</Text>
      </div>
    ),
  },
  {
    key: "totalService",
    dataIndex: "totalService",
    render: (value) => (
      <div>
        <Text>{value}</Text>
        <Text variant="tiny">Dịch vụ</Text>
      </div>
    ),
  },
  {
    key: "totalProduct",
    dataIndex: "totalProduct",
    render: (value) => (
      <div>
        <Text>{value}</Text>
        <Text variant="tiny">Sản phẩm</Text>
      </div>
    ),
  },
  {
    key: "exp",
    dataIndex: "exp",
    render: () => (
      <div>
        <Text>15/07/2022</Text>
        <Text variant="tiny">Ngày hết hạn</Text>
      </div>
    ),
  },
  {
    key: "contractType",
    dataIndex: "contractType",
    render: () => (
      <div>
        <Text>1 tháng</Text>
        <Text variant="tiny">Loại hợp đồng</Text>
      </div>
    ),
  },
  {
    key: "active",
    dataIndex: "active",
    render: () => (
      <div>
        <Switch />
        <Text variant="tiny">Kích hoạt</Text>
      </div>
    ),
  },
  {
    key: "action",
    dataIndex: "action",
    render: () => (
      <StyledWrapAction>
        <EditIcon />
        <DeleteIcon />
      </StyledWrapAction>
    ),
  },
];

export const Stores: React.FC<StoreProps> = ({
  stores: allStores,
  businessModels,
}) => {
  const navigate = useNavigate();

  const [stores, setStores] = useState<SerializeFrom<StoreData[]>>([]);
  const [businessModelSelected, setBusinessModelSelected] = useState<string[]>(
    []
  );

  const onSelectBusinessModel = (id: string) => {
    const businessModelSelectedCloned = [...businessModelSelected];
    const index = businessModelSelectedCloned.indexOf(id);

    if (index >= 0) {
      businessModelSelectedCloned.splice(index, 1);
    } else {
      businessModelSelectedCloned.push(id);
    }

    setBusinessModelSelected(businessModelSelectedCloned);
  };

  const onSearchStoresByName = (value: string) => {
    const storesFiltered = filter(
      (store) =>
        toNonAccentVietnamese(store.name.toLowerCase()).includes(
          toNonAccentVietnamese(value.toLowerCase())
        ),
      allStores
    );

    setStores(storesFiltered);
  };

  const goStoreCreatePage = () => {
    navigate(RoutePath.STORE_CREATE);
  };

  useEffect(() => {
    setStores(allStores);
  }, []);

  return (
    <Layout>
      <Text variant="heading">Cửa hàng</Text>

      <StyledWrapBusinessTags>
        {businessModels.map((businessModel) => (
          <CheckableTag
            key={businessModel.id}
            label={businessModel.name}
            checked={businessModelSelected.includes(businessModel.id)}
            onClick={() => onSelectBusinessModel(businessModel.id)}
          />
        ))}
      </StyledWrapBusinessTags>

      <StyledWrapFilters>
        <Input
          prefix={<SearchIcon />}
          placeholder="Tìm theo tên..."
          onChange={(evt) => onSearchStoresByName(evt.target.value)}
        />

        <StyledWrapDropdownFilters>
          <Text variant="caption" color={theme.colors.light}>
            Lọc theo:
          </Text>

          <Dropdown
            prefixIcon={<CalendarIcon color={theme.colors.gray900} />}
            placeholder="30 ngày"
          />

          <Dropdown
            prefixIcon={<StoreIcon color={theme.colors.gray900} />}
            placeholder="Hồ Chí Minh"
          />

          <Dropdown placeholder="Quận 3" />
        </StyledWrapDropdownFilters>
      </StyledWrapFilters>

      <Table
        rowKey="id"
        showHeader={false}
        columns={columns}
        dataSource={stores}
      />

      <StyledAddButton
        shape="round"
        icon={<PlusIcon color={theme.colors.brand100} />}
        onClick={goStoreCreatePage}
      />
    </Layout>
  );
};
