import styled from "@emotion/styled";

import { theme } from "~/src/common/config";
import { Button, Text } from "~/src/components/molecules";

//---------------------- Stores Page ----------------------//
export const StyledWrapName = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledWrapAvatar = styled.div`
  margin-right: 8px;
`;

export const StyledWrapBusinessTags = styled.div`
  margin-top: 8px;
  margin-bottom: 8px;

  span:nth-of-type(n + 1) {
    margin-top: 8px;
    margin-bottom: 8px;
  }
`;

export const StyledWrapFilters = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  margin-bottom: 4px;

  .ant-input-affix-wrapper {
    max-width: 280px;
  }
`;

export const StyledWrapDropdownFilters = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 8px;
`;

export const StyledWrapAction = styled.div`
  display: flex;
  align-items: center;
  gap: 16px;

  svg:first-of-type {
    :hover {
      path {
        stroke: ${theme.colors.warning600};
      }
    }
  }

  svg:last-of-type {
    :hover {
      path {
        stroke: ${theme.colors.danger600};
      }
    }
  }
`;

export const StyledAddButton = styled(Button)`
  position: fixed;
  bottom: 50px;
  right: 16px;

  display: flex;
  align-items: center;
`;

//---------------------- StoreCreate Page ----------------------//
export const StyledTitle = styled(Text)`
  margin-bottom: 8px;
`;

export const StyledStoreCreate = styled.div`
  width: 682px;
  margin: auto;
`;

export const StyledTitleOfSection = StyledTitle;

export const StyledStoreCreateSection = styled.div`
  margin-bottom: 20px;
  padding: 20px;

  border-radius: ${theme.rounded.xl};
  background: ${theme.colors.white};
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.1);
`;

export const StyledBackButton = styled(Button)`
  position: fixed;
  top: 120px;
  left: 32px;

  display: flex;
  align-items: center;
`;

export const StyledActiveStore = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const StyledSaveButton = StyledAddButton;
