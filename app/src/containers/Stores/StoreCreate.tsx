import { useState } from "react";
import { Col, Row } from "antd";
import { useNavigate } from "@remix-run/react";
import { SerializeFrom } from "@remix-run/node";
import { BusinessModel } from "@prisma/client";

import { Layout } from "~/src/components/organisms/Layout";
import {
  ArrowLeftIcon,
  CheckableTag,
  DatePicker,
  Dropdown,
  FormItem,
  Input,
  Label,
  SaveIcon,
  Switch,
  Text,
} from "~/src/components/molecules";

import {
  StyledActiveStore,
  StyledBackButton,
  StyledSaveButton,
  StyledStoreCreate,
  StyledStoreCreateSection,
  StyledTitle,
  StyledTitleOfSection,
  StyledWrapBusinessTags,
} from "./Stores.styles";
import { theme } from "~/src/common/config";

interface StoreCreateProps {
  businessModels: SerializeFrom<BusinessModel[]>;
}

export const StoreCreate: React.FC<StoreCreateProps> = ({ businessModels }) => {
  const navigate = useNavigate();

  const [businessModelSelected, setBusinessModelSelected] = useState<string[]>(
    []
  );

  const onSelectBusinessModel = (id: string) => {
    const businessModelSelectedCloned = [...businessModelSelected];
    const index = businessModelSelectedCloned.indexOf(id);

    if (index >= 0) {
      businessModelSelectedCloned.splice(index, 1);
    } else {
      businessModelSelectedCloned.push(id);
    }

    setBusinessModelSelected(businessModelSelectedCloned);
  };

  const goPrevPage = () => {
    navigate(-1);
  };

  return (
    <Layout sideNav={false}>
      <StyledStoreCreate>
        <StyledTitle variant="heading">Cửa hàng mới</StyledTitle>

        <StyledStoreCreateSection>
          <StyledTitleOfSection variant="body">
            Thông tin cửa hàng
          </StyledTitleOfSection>

          <Row gutter={12}>
            <Col span={14}>
              <FormItem label="Tên cửa hàng">
                <Input placeholder="Cửa hàng bạn tên gì?" />
              </FormItem>
            </Col>

            <Col span={10}>
              <FormItem label="Số điện thoại">
                <Input placeholder="Liên lạc qua số nào?" />
              </FormItem>
            </Col>
          </Row>

          <Label>Loại hình kinh doanh</Label>
          <StyledWrapBusinessTags>
            {businessModels.map((businessModel) => (
              <CheckableTag
                key={businessModel.id}
                label={businessModel.name}
                checked={businessModelSelected.includes(businessModel.id)}
                onClick={() => onSelectBusinessModel(businessModel.id)}
              />
            ))}
          </StyledWrapBusinessTags>

          <Label>Địa chỉ</Label>
        </StyledStoreCreateSection>

        <StyledStoreCreateSection>
          <StyledTitleOfSection variant="body">
            Thông tin đăng ký
          </StyledTitleOfSection>

          <Row gutter={12}>
            <Col span={12}>
              <FormItem label="Email đăng ký">
                <Input />
              </FormItem>
            </Col>

            <Col span={12}>
              <FormItem label="Mật khẩu">
                <Input />
              </FormItem>
            </Col>
          </Row>

          <Row gutter={12}>
            <Col span={12}>
              <FormItem label="Người quản lý (P.I.C)">
                <Input placeholder="Tên người quản lý / chủ cửa hàng" />
              </FormItem>
            </Col>

            <Col span={12}>
              <FormItem label="Số điện thoại">
                <Input placeholder="Liên lạc qua số nào?" />
              </FormItem>
            </Col>
          </Row>
        </StyledStoreCreateSection>

        <StyledStoreCreateSection>
          <StyledTitleOfSection variant="body">
            Chi tiết hợp đồng
          </StyledTitleOfSection>

          <Row gutter={12}>
            <Col span={12}>
              <FormItem label="Số hợp đồng">
                <Input />
              </FormItem>
            </Col>

            <Col span={12}>
              <FormItem label="Loại hợp đồng">
                <Dropdown />
              </FormItem>
            </Col>
          </Row>

          <Row gutter={12}>
            <Col span={12}>
              <FormItem label="Ngày bắt đầu">
                <DatePicker />
              </FormItem>
            </Col>

            <Col span={12}>
              <FormItem label="Ngày kết thúc">
                <DatePicker />
              </FormItem>
            </Col>
          </Row>

          <FormItem label="Kích hoạt cửa hàng">
            <StyledActiveStore>
              <Text variant="body" weight="medium">
                Cho phép cửa hàng hiện thị trên kênh bán của BeautyBooking.
              </Text>

              <Switch />
            </StyledActiveStore>
          </FormItem>
        </StyledStoreCreateSection>
      </StyledStoreCreate>

      <StyledBackButton
        variant="tertiary"
        shape="round"
        icon={<ArrowLeftIcon color={theme.colors.gray700} />}
        onClick={goPrevPage}
      />

      <StyledSaveButton
        shape="round"
        icon={<SaveIcon color={theme.colors.brand100} />}
      />
    </Layout>
  );
};
