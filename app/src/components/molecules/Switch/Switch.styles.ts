import { Switch } from "antd";
import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

export const StyledSwitch = styled(Switch)`
  height: 20px;
  min-width: 36px;

  background: ${theme.colors.gray400};

  &:hover:not(.ant-switch-disabled) {
    background: ${theme.colors.gray400};
  }

  .ant-switch-handle {
    width: 14px;
    height: 14px;

    top: 3px;
    inset-inline-start: 3px;
  }

  &.ant-switch-checked {
    background: ${theme.colors.brand600};

    &:hover:not(.ant-switch-disabled) {
      background: ${theme.colors.brand600};
    }

    .ant-switch-handle {
      inset-inline-start: calc(100% - 18px);
    }
  }
`;
