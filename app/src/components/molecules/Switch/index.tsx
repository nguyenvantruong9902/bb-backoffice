import { SwitchProps } from "antd";
import { StyledSwitch } from "./Switch.styles";

export const Switch: React.FC<SwitchProps> = ({ ...restProps }) => {
  return <StyledSwitch {...restProps} />;
};
