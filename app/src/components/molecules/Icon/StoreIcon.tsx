import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const StoreIcon: React.FC<CustomSvgProps> = ({
  color,
  ...restProps
}) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...restProps}
    >
      <path
        d="M15.1822 17.3855C16.5636 17.3855 17.6833 16.2659 17.6833 14.8845V8.77113C17.6833 7.99914 17.3273 7.2705 16.7178 6.79697L11.7158 2.90618C10.8129 2.20339 9.5482 2.20339 8.64449 2.90618L3.6424 6.79697C3.03298 7.2705 2.677 7.99914 2.677 8.77113V14.8845C2.677 16.2659 3.79663 17.3855 5.17804 17.3855"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.1821 17.3855H5.17798"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.51279 9.04877V9.88245"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.8475 9.04877V9.88245"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.098 12.957C13.098 12.957 12.0042 14.0508 10.1801 14.0508C8.356 14.0508 7.26221 12.957 7.26221 12.957"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
