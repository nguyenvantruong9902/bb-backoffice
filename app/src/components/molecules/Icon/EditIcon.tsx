import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const EditIcon: React.FC<CustomSvgProps> = ({ color, ...restProps }) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      viewBox="0 0 20 20"
      {...restProps}
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M14.484 3.848v0c.69.691.69 1.81 0 2.502l-8.326 8.325a1.668 1.668 0 01-.774.439l-2.887.722.722-2.887c.073-.293.225-.56.438-.774l8.327-8.327c.69-.69 1.81-.69 2.5 0z"
        clipRule="evenodd"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M12.918 7.916l-2.501-2.501M17.503 15.002l-.912.912a2.58 2.58 0 01-3.647 0v0a2.584 2.584 0 00-3.648 0"
      />
    </svg>
  );
};
