import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const ArrowDownIcon: React.FC<CustomSvgProps> = ({
  color,
  ...restProps
}) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...restProps}
    >
      <path
        d="M6.66675 8.33331L10.0001 11.6666L13.3334 8.33331"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
