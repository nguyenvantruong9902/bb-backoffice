import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const ArrowLeftIcon: React.FC<CustomSvgProps> = ({
  color,
  ...restProps
}) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      width="20"
      height="21"
      viewBox="0 0 20 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...restProps}
    >
      <path
        d="M3.34155 10.9208H15.8332"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.34413 5.92749L3.33496 10.9375L8.34413 15.9475"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
