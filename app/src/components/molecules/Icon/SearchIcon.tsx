import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const SearchIcon: React.FC<CustomSvgProps> = ({
  color,
  ...restProps
}) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...restProps}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.21552 15.1005C12.4657 15.1005 15.1005 12.4657 15.1005 9.21552C15.1005 5.96535 12.4657 3.33057 9.21552 3.33057C5.96535 3.33057 3.33057 5.96535 3.33057 9.21552C3.33057 12.4657 5.96535 15.1005 9.21552 15.1005Z"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.6695 16.6694L13.3765 13.3764"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
