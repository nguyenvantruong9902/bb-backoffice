import { SVGProps } from "react";

export interface CustomSvgProps extends SVGProps<SVGSVGElement> {
  color?: string;
}
