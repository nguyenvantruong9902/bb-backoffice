import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const DeleteIcon: React.FC<CustomSvgProps> = ({
  color,
  ...restProps
}) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      viewBox="0 0 20 20"
      {...restProps}
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M15.443 7.5H4.557a.856.856 0 01-.857-.856v-.788c0-.473.383-.856.857-.856h10.887c.473 0 .856.383.856.856v.787a.856.856 0 01-.857.857v0z"
        clipRule="evenodd"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M15 7.5l-.706 8.472a1.667 1.667 0 01-1.66 1.528H7.366c-.867 0-1.59-.664-1.661-1.528L5 7.5M6.667 5l1.02-2.04c.14-.282.429-.46.745-.46h3.136c.316 0 .604.178.745.46L13.333 5M10 10v5M12.5 10l-.342 5M7.5 10l.342 5"
      />
    </svg>
  );
};
