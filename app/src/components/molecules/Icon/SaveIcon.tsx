import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const SaveIcon: React.FC<CustomSvgProps> = ({ color, ...restProps }) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...restProps}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.552 4H6.67601C5.19601 4 3.99868 5.20533 4.00935 6.68667L4.14801 25.3533C4.15868 26.8187 5.34935 28 6.81468 28H25.3227C26.796 28 27.9894 26.8067 27.9894 25.3333V10.4373C27.9894 9.73067 27.708 9.052 27.208 8.552L23.4373 4.78133C22.9373 4.28133 22.26 4 21.552 4Z"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M21.3239 4V9.212C21.3239 9.948 20.7266 10.5453 19.9906 10.5453H11.9906C11.2546 10.5453 10.6572 9.948 10.6572 9.212V4"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.3335 28V17.7147C9.3335 16.768 10.1015 16 11.0482 16H20.9535C21.8988 16 22.6668 16.768 22.6668 17.7147V28"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
