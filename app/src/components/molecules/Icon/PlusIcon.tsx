import { theme } from "~/src/common/config";

import { CustomSvgProps } from "./types";

export const PlusIcon: React.FC<CustomSvgProps> = ({ color, ...restProps }) => {
  const stroke = color ?? theme.colors.gray500;

  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...restProps}
    >
      <path
        d="M5.33325 16H26.6666"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.9999 26.6667V5.33333"
        stroke={stroke}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
