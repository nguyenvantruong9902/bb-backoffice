import { useEffect, useRef, useState } from "react";
import { Select, SelectProps } from "antd";
import { map } from "lodash/fp";

import { theme } from "~/src/common/config";

import { Text } from "../Text";
import { ArrowDownIcon, ArrowUpIcon } from "../Icon";

import { StyledDropdown, StyledPrefix } from "./Dropdown.styles";

interface DropdownProps extends SelectProps {
  prefixIcon?: React.ReactNode;
}

export const Dropdown: React.FC<DropdownProps> = ({
  prefixIcon,
  options,
  ...restProps
}) => {
  const dropdownRef = useRef<HTMLDivElement>(null);

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClickOutside = (event: MouseEvent) => {
    if (
      dropdownRef.current &&
      !dropdownRef.current.contains(event.target as Node)
    ) {
      setIsOpen(false);
    }
  };

  const calculateDropdownHeight = (): string => {
    if (!options) return "0px";

    if (options.length <= 3) {
      return `calc(42px * ${options.length} + 32px)`;
    }

    return `calc(42px * 3 + 37px)`;
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);

    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, [dropdownRef]);

  return (
    <StyledDropdown
      ref={dropdownRef}
      isPrefix={!!prefixIcon}
      onClick={() => setIsOpen(!isOpen)}
    >
      {prefixIcon && <StyledPrefix>{prefixIcon}</StyledPrefix>}

      <Select
        open={isOpen}
        suffixIcon={isOpen ? <ArrowUpIcon /> : <ArrowDownIcon />}
        dropdownStyle={{
          height: calculateDropdownHeight(),
          padding: "16px",
          borderRadius: theme.rounded.xl,
        }}
        getPopupContainer={(triggerNode) => triggerNode.parentElement}
        {...restProps}
      >
        {map(({ label, value }) => {
          return (
            <Select.Option key={value}>
              <Text variant="paragraph" color={theme.colors.medium}>
                {label}
              </Text>
            </Select.Option>
          );
        }, options)}
      </Select>
    </StyledDropdown>
  );
};
