import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

interface StyledDropdownProps {
  isPrefix: boolean;
}

export const StyledDropdown = styled.div<StyledDropdownProps>`
  position: relative;

  .ant-select {
    width: 100%;

    .ant-select-selector {
      border-radius: ${theme.rounded.lg};

      height: 40px;
      padding-top: 4px;
      padding-bottom: 4px;
      padding-right: 16px;
      padding-left: ${({ isPrefix }) => (isPrefix ? "48px" : "16px")};

      box-shadow: none !important;
      border-color: ${theme.colors.gray400};

      .ant-select-selection-search-input {
        height: 100%;
      }

      .ant-select-selection-placeholder {
        font-size: 14px;
        font-weight: 500;
        color: ${theme.colors.light};
      }

      :hover,
      :focus {
        border-color: ${theme.colors.brand500} !important;
      }
    }

    &.ant-select-focused .ant-select-selector {
      border-width: 2px;
      border-color: ${theme.colors.brand500} !important;

      + .ant-select-arrow {
        svg path {
          stroke: ${theme.colors.brand600};
        }
      }
    }

    .ant-select-item.ant-select-item-option {
      padding: 8px 12px;
      border-radius: ${theme.rounded.lg};

      background-color: ${theme.colors.white};

      :hover,
      &.ant-select-item-option-active {
        background-color: rgba(10, 31, 68, 0.03); // Hover / On White

        p {
          color: ${theme.colors.dark};
        }
      }
    }
  }
`;

export const StyledPrefix = styled.div`
  position: absolute;
  left: 16px;
  top: 10px;
  z-index: 1;

  cursor: pointer;
`;
