import styled from "@emotion/styled";

import { Align } from ".";

interface StyledTextProps {
  color: string;
  textAlign: Align;
  fontSize: string;
  lineHeight: string;
  fontWeight: string;
}

export const StyledText = styled.p<StyledTextProps>`
  color: ${(props) => props.color};
  text-align: ${(props) => props.textAlign};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
`;
