import React, { HTMLAttributes } from "react";
import { assign } from "lodash/fp";

import { theme } from "~/src/common/config";
import { ValuesOf } from "~/src/utils";
import { StyledText } from "./Text.styles";

const { disabled, dark, light, medium } = theme.colors;

export interface TextSize {
  textWeight: ValuesOf<typeof TextWeightMap>;
  textColor: TextColor;
  lineHeight: string;
  fontSize: string;
}

export type TextVariant =
  | "heading"
  | "title"
  | "subheader"
  | "paragraph"
  | "body"
  | "caption"
  | "tiny";
export type TextWeight =
  | "bold2"
  | "bold"
  | "semibold"
  | "medium"
  | "regular"
  | "light";
export type TextColor = "disabled" | "light" | "medium" | "dark";
export type Align = "left" | "center" | "right" | "justify";

export const TextWeightMap: Record<TextWeight, string> = {
  bold2: "800",
  bold: "700",
  semibold: "600",
  medium: "500",
  regular: "400",
  light: "300",
};

export const TextColorMap: Record<TextColor, string> = {
  disabled,
  dark,
  light,
  medium,
};

export const TextVariantMap: Record<TextVariant, TextSize> = {
  heading: {
    textWeight: TextWeightMap.bold,
    textColor: "dark",
    lineHeight: "36px",
    fontSize: "24px",
  },
  title: {
    textWeight: TextWeightMap.bold,
    textColor: "dark",
    lineHeight: "32px",
    fontSize: "22px",
  },
  subheader: {
    textWeight: TextWeightMap.semibold,
    textColor: "dark",
    lineHeight: "30px",
    fontSize: "17px",
  },
  paragraph: {
    textWeight: TextWeightMap.medium,
    textColor: "dark",
    lineHeight: "26px",
    fontSize: "14px",
  },
  body: {
    textWeight: TextWeightMap.medium,
    textColor: "medium",
    lineHeight: "24px",
    fontSize: "14px",
  },
  caption: {
    textWeight: TextWeightMap.medium,
    textColor: "medium",
    lineHeight: "20px",
    fontSize: "12px",
  },
  tiny: {
    textWeight: TextWeightMap.medium,
    textColor: "light",
    lineHeight: "16px",
    fontSize: "10px",
  },
};

interface TextProps extends HTMLAttributes<HTMLParagraphElement> {
  children: React.ReactNode;
  variant?: TextVariant;
  align?: Align;
  color?: TextColor | string;
  weight?: TextWeight;
}

export const Text: React.FC<TextProps> = ({
  children,
  variant = "body",
  align = "left",
  color,
  weight,
  ...restProps
}) => {
  const { textColor, textWeight, lineHeight, fontSize } =
    TextVariantMap[variant];

  const variantProps = {
    color: color ?? TextColorMap[textColor],
    textAlign: align,
    fontSize,
    lineHeight,
    fontWeight: weight ? TextWeightMap[weight] : textWeight,
  };

  const forwardedProps = assign(
    {
      ...variantProps,
    },
    restProps
  );

  return <StyledText {...forwardedProps}>{children}</StyledText>;
};
