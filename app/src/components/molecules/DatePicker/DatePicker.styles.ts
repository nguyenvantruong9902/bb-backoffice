import { DatePicker } from "antd";
import { css } from "@emotion/react";
import styled from "@emotion/styled";

export const datePickerClassName = css`
  border-radius: 8px;

  .ant-picker-panel-container {
    border-radius: 8px;
  }

  .ant-picker-cell .ant-picker-cell-inner {
    border-radius: 50%;
  }

  .ant-picker-cell-in-view.ant-picker-cell-today
    .ant-picker-cell-inner::before {
    border: none;
  }

  .ant-picker-cell-in-view.ant-picker-cell-today .ant-picker-cell-inner {
    border-radius: 50%;
    background-color: #ececff;
    color: #4340fc;
  }

  .ant-picker-header-view button,
  .ant-picker-header-view button:hover {
    color: rgba(0, 0, 0, 0.25);
  }

  .ant-picker-input > input {
    font-weight: 500;
    color: rgb(78, 93, 120);
    border-radius: 6px;
  }

  .ant-picker.ant-picker-disabled {
    background: none;
  }
`;

export const StyledDatePicker = styled(DatePicker)`
  ${datePickerClassName}
  height: 40px;
  width: 100%;
  border-radius: 8px;
`;
