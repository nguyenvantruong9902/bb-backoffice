import { DatePickerProps } from "antd";
import viVN from "antd/lib/date-picker/locale/vi_VN";

import { CalendarIcon } from "../Icon";

import { StyledDatePicker } from "./DatePicker.styles";

export const DatePicker: React.FC<DatePickerProps> = ({ ...restProps }) => {
  return (
    <StyledDatePicker
      locale={viVN}
      suffixIcon={<CalendarIcon />}
      {...restProps}
    />
  );
};
