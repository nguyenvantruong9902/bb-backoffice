import { TableProps as AntdTableProps } from "antd/es/table";

import { StyledTable } from "./Table.styles";

// eslint-disable-next-line
export const Table = (props: AntdTableProps<any>): JSX.Element => {
  return <StyledTable {...props} pagination={false} />;
};
