import styled from "@emotion/styled";
import { Table } from "antd";
import { theme } from "~/src/common/config";

export const StyledTable = styled(Table)`
  .ant-table table {
    border-collapse: separate;
    border-spacing: 0 8px;

    background-color: ${theme.colors.gray200};
  }

  /* ------------------------------ Table header ------------------------------ */

  .ant-table-thead > tr > th {
    padding: 8px 16px 0 16px;

    background-color: transparent;
    border-bottom-color: transparent;

    // H100 - Tiny / Medium
    // Solid / Black / Black-300
    font-size: ${theme.textSize.size.tiny};
    font-weight: 500;
    line-height: ${theme.textSize.height.tiny};
    color: ${theme.colors.black300};

    &::before {
      background-color: transparent !important;
    }
  }

  /* -------------------------------- Table row ------------------------------- */

  .ant-table-tbody > tr {
    background-color: ${theme.colors.white};
    border-radius: ${theme.rounded.lg};

    td {
      padding: 14px 16px;

      border-top: 1px solid ${theme.colors.gray400};
      border-bottom: 1px solid ${theme.colors.gray400};

      :first-of-type {
        border: 1px solid ${theme.colors.gray400};
        border-bottom-left-radius: ${theme.rounded.lg};
        border-top-left-radius: ${theme.rounded.lg};
        border-right: none;
      }

      :last-child {
        border: 1px solid ${theme.colors.gray400};
        border-bottom-right-radius: ${theme.rounded.lg};
        border-top-right-radius: ${theme.rounded.lg};
        border-left: none;
      }
    }

    :hover > td {
      background-color: transparent;
    }
  }

  /* ----------------------------- Table behavior ----------------------------- */

  .ant-table-tbody > .ant-table-row:hover {
    box-shadow: 0px 6px 6px rgba(0, 0, 0, 0.08);
    cursor: pointer;

    td {
      border: 1px solid ${theme.colors.brand600};
      border-right: none;
      border-left: none;

      :first-of-type {
        border: 1px solid ${theme.colors.brand600};
        border-right: none;
      }

      :last-child {
        border: 1px solid ${theme.colors.brand600};
        border-left: none;
      }
    }
  }
`;
