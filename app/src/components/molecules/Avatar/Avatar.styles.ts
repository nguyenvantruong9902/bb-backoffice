import { Avatar } from "antd";
import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

export const StyledAvatar = styled(Avatar)`
  border-radius: ${({ shape }) =>
    shape === "square" ? theme.rounded.xl : "50%"};
  box-shadow: ${theme.shadows[3]};
`;
