import { AvatarProps as AntAvatarProps } from "antd/lib/avatar";
import { UserOutlined } from "@ant-design/icons";
import { assign } from "lodash/fp";
import { StyledAvatar } from "./Avatar.styles";

export type AvatarProps = AntAvatarProps;

export const Avatar: React.FC<AvatarProps> = ({
  shape = "square",
  size,
  ...restProps
}) => {
  const forwardedProps = assign({}, restProps);

  return (
    <StyledAvatar
      shape={shape}
      size={size ?? 60}
      icon={<UserOutlined />}
      {...forwardedProps}
    />
  );
};
