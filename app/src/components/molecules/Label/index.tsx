/* eslint-disable security/detect-object-injection */

import { LabelHTMLAttributes } from "react";
import { assign } from "lodash/fp";

import {
  TextColor,
  TextColorMap,
  TextSize,
  TextVariantMap,
  TextWeightMap,
} from "../Text";
import { StyledLabel } from "./Label.styles";

type LabelVariant = Extract<TextColor, "light" | "dark">;

export interface LabelProps extends LabelHTMLAttributes<HTMLLabelElement> {
  children: React.ReactNode;
  variant?: LabelVariant;
}

const LabelVariantMap: Record<
  LabelVariant,
  { textSize: TextSize; color: string }
> = {
  light: {
    color: TextColorMap["light"],
    textSize: TextVariantMap["caption"],
  },
  dark: {
    color: TextColorMap["dark"],
    textSize: TextVariantMap["body"],
  },
};

export const Label: React.FC<LabelProps> = ({
  children,
  variant = "light",
  ...restProps
}) => {
  const textWeight: string = TextWeightMap.medium;
  const {
    color,
    textSize: { fontSize, lineHeight },
  } = LabelVariantMap[variant];

  const forwardedProps = assign(
    {
      color,
      fontWeight: textWeight,
      fontSize,
      lineHeight,
    },
    restProps
  );

  return <StyledLabel {...forwardedProps}>{children}</StyledLabel>;
};
