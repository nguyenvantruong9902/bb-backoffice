import styled from "@emotion/styled";

import { TextSize } from "../Text";

interface StyledLabelProps {
  color: string;
  fontSize: string;
  fontWeight: string;
  lineHeight: string;
}

export const StyledLabel = styled.label<StyledLabelProps>`
  margin-bottom: 4px;

  color: ${({ color }) => color};
  font-size: ${({ fontSize }) => fontSize};
  line-height: ${({ lineHeight }) => lineHeight};
  font-weight: ${({ fontWeight }) => fontWeight};
`;
