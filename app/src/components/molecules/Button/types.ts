import { ButtonProps as AntdButtonProps } from "antd";

export type ButtonType = "tertiary" | "warning";

export interface ButtonProps extends AntdButtonProps {
  variant?: ButtonType;
}
