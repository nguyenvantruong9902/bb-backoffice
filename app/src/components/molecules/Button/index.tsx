import { StyledButton } from "./Button.styles";
import { ButtonProps } from "./types";

export const Button: React.FC<ButtonProps> = ({ ...restProps }) => {
  return <StyledButton {...restProps} />;
};
