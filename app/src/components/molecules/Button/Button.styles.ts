import { Button } from "antd";
import { css } from "@emotion/react";
import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

import { ButtonProps } from "./types";

const primaryStyles = css`
  background: ${theme.colors.brand600};
  box-shadow: 0px 6px 6px rgba(0, 0, 0, 0.08);
`;

const tertiaryStyles = css`
  background: ${theme.colors.white};
`;

const warningStyles = css``;

export const StyledButton = styled(Button)<ButtonProps>`
  &.ant-btn {
    width: auto;
    height: auto;
    padding: 12px;

    border: none;
  }

  ${({ variant }) => {
    switch (variant) {
      case "tertiary":
        return tertiaryStyles;

      case "warning":
        return warningStyles;

      default:
        return primaryStyles;
    }
  }}
`;
