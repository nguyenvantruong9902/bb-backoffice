import { InputProps as AntdInputProps } from "antd";

import { StyledInput } from "./Input.styles";

interface InputProps extends AntdInputProps {
  //
}

export const Input: React.FC<InputProps> = ({ ...restProps }) => {
  return <StyledInput {...restProps} />;
};
