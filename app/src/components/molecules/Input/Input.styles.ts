import { Input } from "antd";
import { css } from "@emotion/react";
import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

const inputCss = css`
  font-size: 14px;
  font-weight: 500;
  color: ${theme.colors.medium};

  ::placeholder {
    color: ${theme.colors.disabled};
  }
`;

export const StyledInput = styled(Input)`
  height: 40px;
  padding: 8px 16px;

  border-radius: ${theme.rounded.lg};
  border-color: ${theme.colors.gray400};
  box-shadow: none;

  ${inputCss}

  :hover {
    border-color: ${theme.colors.brand500} !important;
  }

  :focus,
  &.ant-input-affix-wrapper-focused {
    border-width: 2px;
    border-color: ${theme.colors.brand500} !important;

    box-shadow: none;

    :hover {
      border-width: 2px;
    }
  }

  input {
    ${inputCss}
  }
`;
