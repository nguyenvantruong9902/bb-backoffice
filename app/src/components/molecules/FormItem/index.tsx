import { HTMLAttributes } from "react";

import { theme } from "~/src/common/config";

import { Text } from "../Text";
import { StyledFormItem } from "./FormItem.styles";

type BottomSpacing = "none" | "sm" | "md" | "lg" | "xlg" | "xxlg";

const BottomSpacingMap: Record<BottomSpacing, string> = {
  none: "0px",
  sm: "8px",
  md: "12px",
  lg: "24px",
  xlg: "32px",
  xxlg: "40px",
};

export interface FormItemProps extends HTMLAttributes<HTMLDivElement> {
  label?: string;
  children: React.ReactNode;
  bottomSpacing?: BottomSpacing;
}

export const FormItem: React.FC<FormItemProps> = ({
  label,
  children,
  bottomSpacing = "md",
}) => {
  return (
    <StyledFormItem bottomSpacing={BottomSpacingMap[bottomSpacing]}>
      {label && (
        <Text
          variant="caption"
          color={theme.colors.light}
          style={{ marginBottom: "4px" }}
        >
          {label}
        </Text>
      )}

      {children}
    </StyledFormItem>
  );
};
