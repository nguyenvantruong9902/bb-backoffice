import styled from "@emotion/styled";

interface StyledFormItemProps {
  bottomSpacing: string;
}

export const StyledFormItem = styled.div<StyledFormItemProps>`
  margin-bottom: ${({ bottomSpacing }) => bottomSpacing};
`;
