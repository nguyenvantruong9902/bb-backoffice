import { Tag } from "antd";
import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

interface StyledTagProps {
  checked: boolean;
}

const bgColor = (checked: boolean) =>
  checked ? theme.colors.brand700 : theme.colors.gray300;
const textColor = (checked: boolean) =>
  checked ? theme.colors.white : theme.colors.medium;
const hoverBgColor = (checked: boolean) => (checked ? "#4C4CE8" : "#F8F8FF");

export const StyledTag = styled(Tag)<StyledTagProps>`
  cursor: pointer;

  border: none;
  border-radius: ${theme.rounded.lg};
  background: ${({ checked }) => bgColor(checked)};
  padding: 10px;

  font-weight: 500;
  color: ${({ checked }) => textColor(checked)};
  line-height: 22px;
  font-size: 14px;

  &:hover {
    background: ${({ checked }) => hoverBgColor(checked)};
  }
}
`;
