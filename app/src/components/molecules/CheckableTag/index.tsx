import { HTMLAttributes } from "react";
import { assign } from "lodash/fp";

import { StyledTag } from "./CheckableTag.styles";

export interface CheckableTagProps extends HTMLAttributes<HTMLSpanElement> {
  label: string;
  checked?: boolean;
}

export const CheckableTag: React.FC<CheckableTagProps> = ({
  label,
  checked = false,
  ...restProps
}) => {
  const forwardedProps = assign(
    {
      checked,
    },
    restProps
  );

  return <StyledTag {...forwardedProps}>{label}</StyledTag>;
};
