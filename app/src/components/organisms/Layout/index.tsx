import { SideNav } from "./SideNav";
import { StyledContent, StyledLayout } from "./styles";

interface LayoutProps {
  sideNav?: boolean;
  children: React.ReactNode;
}

export const Layout: React.FC<LayoutProps> = ({ sideNav = true, children }) => {
  return (
    <StyledLayout>
      {sideNav && <SideNav />}

      <StyledContent>{children}</StyledContent>
    </StyledLayout>
  );
};
