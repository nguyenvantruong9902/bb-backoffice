import {
  Avatar,
  DashboardIcon,
  PromotionIcon,
  SpeakerIcon,
  StoreIcon,
} from "../../molecules";

import {
  StyledSideNav,
  StyledLogo,
  NavigationSegment,
  StyledNavigation,
} from "./styles";
import { Link } from "@remix-run/react";
import { RoutePath } from "~/src/utils";

export const SideNav = () => {
  return (
    <StyledSideNav>
      <div>
        <Link to={RoutePath.STORES}>
          <StyledLogo src="/logo.svg" alt="logo" />
        </Link>

        <NavigationSegment>
          <StyledNavigation to={RoutePath.DASHBOARD}>
            <DashboardIcon />
          </StyledNavigation>

          <StyledNavigation to={RoutePath.STORES}>
            <StoreIcon color="#A2A7BA" />
          </StyledNavigation>

          <StyledNavigation to={RoutePath.PROMOTION}>
            <PromotionIcon />
          </StyledNavigation>

          <StyledNavigation to={RoutePath.SPEAKER}>
            <SpeakerIcon />
          </StyledNavigation>
        </NavigationSegment>
      </div>

      <Avatar size={40} shape="circle" src="https://picsum.photos/200/300" />
    </StyledSideNav>
  );
};
