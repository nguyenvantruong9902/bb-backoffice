import { NavLink } from "@remix-run/react";
import styled from "@emotion/styled";

import { theme } from "~/src/common/config";

export const StyledSideNav = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  height: 100vh;
  padding: 24px 18px;

  background-color: ${theme.colors.white};
`;

export const StyledLayout = styled.div`
  display: flex;
`;

export const StyledLogo = styled.img`
  cursor: pointer;
`;

export const NavigationSegment = styled.div`
  display: flex;
  gap: 12px;
  flex-direction: column;

  margin-top: 48px;

  & > a:nth-of-type(1),
  & > a:nth-of-type(3),
  & > a:nth-of-type(4) {
    &.active > svg path,
    &:hover > svg path {
      fill: ${theme.colors.brand600};
    }
  }

  & > a:nth-of-type(2) {
    &.active > svg path,
    &:hover > svg path {
      stroke: ${theme.colors.brand600};
    }
  }
`;

export const StyledNavigation = styled(NavLink)`
  height: 44px;

  display: flex;
  align-items: center;
  justify-content: center;

  &.active,
  &:hover {
    border-radius: ${theme.rounded.lg};
    background: ${theme.colors.brand100};
  }
`;

export const StyledContent = styled.div`
  height: 100vh;
  width: 100%;
  overflow: auto;
  padding: 32px;
`;
