export enum RoutePath {
  DASHBOARD = "/dashboard",
  STORES = "/stores",
  STORE_CREATE = "/stores/create",
  PROMOTION = "/promotion",
  SPEAKER = "/speaker",
}
