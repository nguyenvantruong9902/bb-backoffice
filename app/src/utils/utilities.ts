import { attempt, isArray, isError, isNil, map, negate } from "lodash/fp";

export const isStringifiedJSON = (str: string): boolean =>
  !isError(attempt(() => JSON.parse(str)));

export const isNotNil = negate(isNil);
