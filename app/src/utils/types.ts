import { Store } from "@prisma/client";

export type ValuesOf<T> = T[keyof T];
export type Nullable<T> = T | null;

export interface StoreData extends Store {
  totalService: number;
  totalProduct: number;
}
