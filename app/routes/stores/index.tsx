import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";

import { Stores } from "~/src/containers/Stores";

import { getAllStores, getBusinessModels } from "~/services";

export async function loader() {
  const [stores, businessModels] = await Promise.all([
    getAllStores(),
    getBusinessModels(),
  ]);

  return json({ stores, businessModels });
}

export default function StoresPage() {
  const { stores, businessModels } = useLoaderData<typeof loader>();

  return <Stores stores={stores} businessModels={businessModels} />;
}
