import { json } from "@remix-run/node";

import { StoreCreate } from "~/src/containers/Stores";

import { getBusinessModels } from "~/services";
import { useLoaderData } from "@remix-run/react";

export async function loader() {
  const businessModels = await getBusinessModels();

  return json({ businessModels });
}

export default function StoreCreatePage() {
  const { businessModels } = useLoaderData<typeof loader>();

  return <StoreCreate businessModels={businessModels} />;
}
