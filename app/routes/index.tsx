import { redirect } from "@remix-run/node";

import { RoutePath } from "~/src/utils";

export async function loader() {
  return redirect(RoutePath.STORES);
}

export default function Index() {
  return null;
}
